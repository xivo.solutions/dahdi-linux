# DAHDI-Linux with DKMS

**Link to the full documentation:** https://gitlab.com/avencall/randd-doc/wikis/dkms

## Link with dahdi official repo

- This project contains the packaging for [dahdi linux](https://github.com/asterisk/dahdi-linux).

## How to update 

If you have to update this project (basically if you need to have a new dahdi version):
1. Find the upstream release you need,
2. Change the version in `VERSION` file according to the upstream release version
3. Then check that the version put in `VERSION` and the DOWNLOAD_URL constructed in `debian/rules` match the upstream release
4. Then go to the builder
5. 

## Introduction: XiVO and DKMS
This folder contains XiVO's version of DAHDI-Linux. It has been restructured in order to be able to work with DKMS.

DKMS is a framework which allows automatic rebuilding of modules when a new kernel is installed. Getting those modules to build with DKMS allows them to build themselves automatically at every kernel update, thus saving time (as long as the modules are compatible with the current kernel version).

## Build requirements

- Debian 12
- devscripts
- dpkg-dev
- dkms
- xivo-build-tools: branch 4825-build-asterisk-deb11

## Instructions

Build the modules before generating a config:
```bash
make
```

Generate a config:
```bash
sudo build_tools/dkms-helper generate_conf > dkms.conf
```

Clean the configuration:
```bash
make clean
```

Copy everything in usr:
```bash
sudo mkdir -p /usr/src/dahdi-linux-3.1.0
sudo cp -R . /usr/src/dahdi-linux-3.1.0
```

**DKMS Remove** (Done just in case)
```bash
sudo dkms remove -m dahdi-linux -v 3.1.0 --all
```

**DKMS Add**
```bash
sudo dkms add -m dahdi-linux -v 3.1.0
```

**DKMS Build**
```bash
sudo dkms build -m dahdi-linux -v 3.1.0
```

**DKMS Mkdeb**

For this command to work, you have to install the following packages: ```fakeroot```, ```debhelper```
```bash
sudo dkms mkdeb -m dahdi-linux -v 3.1.0 --source-only
```

The .deb file is in ```/var/lib/dkms/dahdi-linux/3.1.0/deb/```.
```bash
cd ..
cp /var/lib/dkms/dahdi-linux/3.1.0/deb/dahdi-linux-dkms_3.1.0_amd64.deb .
```

→ At this point, if everything went smoothly, the .deb package was generated.
On any machine, you can then proceed and install dahdi-linux DKMS:
```bash
sudo dpkg -i dahdi-linux-dkms_3.1.0_amd64.deb
```
